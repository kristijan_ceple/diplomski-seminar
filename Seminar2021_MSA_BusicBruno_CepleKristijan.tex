\documentclass[times, utf8, seminar, numeric]{fer}
\usepackage{booktabs}
\usepackage{url}
\usepackage[export]{adjustbox}
\usepackage{graphicx}
\usepackage{hyperref}

\begin{document}

% Ukljuci literaturu u seminar
\nocite{*}
\title{Poravnavanje više slijedova}
\author{Bruno Bušić, Kristijan Čeple}
\voditelj{Mirjana Domazet-Lošo}
\maketitle

\tableofcontents

\chapter{Uvod}
Poravnavanje slijedova\engl{Sequence Alignment} se koristi u bioinformatici za poravnavanje dva(ili više, što je uvelike zahtijevniji problem) slijedova DNA, RNA ili proteina(aminokiseline), a u svrhu identificiranja regija sličnosti i razlika. Ukoliko govorimo o poravnavanju 2 slijeda kažemo da se radi o poravnanju para slijedova\engl{Pairwise Alignment}. Naravno, moguće je obavljati i poravnavanje više slijedova odjednom, pa onda kažemo da se radi o poravnanju više slijedova\engl{Multiple Sequence Alignment - MSA}.

Svrha poravnavanja slijedova je identificiranje regija sličnosti, jer se pretpostavlja da regije koje su slične obavljaju sličnu/istu funkciju ili imaju sličnu/istu strukturu, pa se mogu koristiti za pronalaženje homolognih gena. Osim toga, moguće je utvrditi i određene evolucijske povezanosti među vrstama, otkriti aktivna mjesta, konstruirati filogenetska stabla, pronaći jedno-nukleotidne polimorfizme\engl{Single-Nucleotide Polymorphisms}, razumjeti određene RNA mehanizme\engl{Alternative RNA Splicing, RNA Editing}, sastavljati genome, itd...

Naravno, poravnavanje para slijedova je računski veoma zahtjevan i složen posao, a poravnavanje više slijedova je onda tek višestruko složeniji. Zato se, posebice kod velikog broja slijedova i/ili dugačkih sliejdova, umjesto korištenja algoritama koji uvijek daju optimalna rješenja često pribjegava heurističkim metodama koje daju sub-optimalna rješenja pritom nastojeći dati što kvalitetnija rješenja - balansirajući željeni omjer kvalitete i performanse.

Pošto je poravnavanje više slijedova "nadogradnja" nad poravnavanjem para slijedova, prirodno je prvo obrazložiti i proći poravnavanje para slijedova, a zatim sa znanjem i razumijevanjem toga koncepta preći na poravnavanje više slijedova. Algoritmi i metode korišteni za poravnavanje bioloških slijedova vuku korijenje iz matematike i računalne znanosti, a njihova primjena ide i van dosega bioinformatike - pa se tako slične metode mogu koristiti i pri računanju cijene udaljenosti među nizovima u obradi prirodnog jezika\engl{Natural Language Processing}, društvenim znanostima, ili čak i u financijskim podacima.

Na primjer, Needleman-Wunsch algoritam se u društvenim znanostima naziva optimalno podudaranje\engl{Optimal matching}. Tehnike koje generiraju set elemenata od kojih će biti izabrane riječi u algoritmima generacije prirodnog jezika dijele sličnosti sa tehnikama poravnavanja više slijedova iz bioinformatike kako bi se proizvela jezična verzija računalno generiranih matematičkih dokaza. U području povijesne i komparativne lingvistike, poravnavanje slijedova se koristi u svrhu automatiziranja(doduše, ne potpunog nego samo djelomičnog) komparativne metode rekonstrukcije jezika. Poravnavanje više slijedova se koristi za konstrukciju filogenetskih stabala te otkrivanje zajedničkih predaka, tako se na isti način mogu unazad rekonstruirati jezici-preci gledajući sličnosti u jezicima-djeci. Za kraj uvoda, metode i algoritmi poravnavanja više slijedova se također koriste u ekonomici u području poslovnog i tržišnog istraživanja i analize.

\chapter{Poravnavanje para slijedova}
Poravnavanje para slijedova se koristi za poravnavanje 2 biološka slijeda - bilo DNA, RNA, ili proteina(aminokiseline). Uobičajen način za reprezentaciju slijedova je pomoću matrice, gdje redak 1 sadrži slijed 1, redak 2 sadrži slijed 2, itd... Između elemenata se umeću praznine\engl{gaps}, pritom pokušavajući u istom stupcu imati iste ili što sličnije elemente. Primjer jedne matrice s obavljenim poravnanjem(Poravnanje više slijedova) slijedi na slijedećoj slici:
\begin{figure}[h]
	\centerline{\includegraphics[max width=\textwidth]{slike/histoni_sisavaca_poravnanje.png}}
	\caption{Primjer poravnavanja više slijedova - Matrica~\cite{slika-histoni_sisavaca_poravnanje}}
	\label{fig:histoni_sisavaca_poravnanje}
\end{figure}

Značenje i razlog poravnavanja slijedova je pokušaj utvrđivanja nekakve poveznice - pokušava se otkriti funkcionalna, strukturalna, evolucijska ili nekakva druga poveznica između 2 slijeda. Pod pretpostavkom da vrste čije slijedove poravnavamo dijele zajedničkog pretka(što je uvijek istinito u slučaju života na Zemlji) onda će dijelovi koji se razlikuju u znaku predstavljati nekakve mutacije, a praznine možemo tumačiti kao \emph{indele}\engl{insertion or deletion}, tj. mutacije brisanja ili dodavanja nizova znakova(bilo u jednom nizu ili u drugom nizu).

Tokom poravnavanja DNA imamo na raspolaganju 4 moguće baze: A, C, G i T, a tokom poravnavanja RNA imamo na raspolaganju isto 4 moguće baze: A, C, G i U. No, kod poravnavanja proteina treba uzeti u obzir da na raspolaganju imamo 20-22 aminokiseline, a one međusobno mogu biti u srodstvu različitih stupnjeva. S obzirom na stupanj srodstva aminokiselina koje se nalaze na istom indeksu/poziciji, možemo otprilike reći koje su regije očuvane(i do koje mjere su očuvane) među vrstama čiji se slijedovi poravnavaju. Ukoliko nema substitucija, ili su substitucije veoma konzervativne prirode, možemo zaključiti da je ta regija slijeda važna - bilo strukturalno ili funkcionalno. Sličan zaključak vrijedi i kod očuvanih regija DNA/RNA slijedova.

Kada pričamo o stupnju sličnosti između aminokiselina, pod veoma slične aminokiseline se zapravo misli na aminokiseline čiji bočni lanci imaju slična biokemijska svojstva. Isto tako, treba se prisjetiti da proteini i nukleinske kiseline su karakterizirani određenim strukturama(primarna, sekundarna, tercijarna, kvartarna).

Tradicionalno bi bilo potrebno biološke slijedove ručno poravnavati, međutim to poravnavanje pokušava se automatizirati pomoću računala, što uvelike olakšava i ubrzava postupak. No, čak i za računala deterministički postupci koji daju optimalna rješenja znaju biti vremenski i računalno prezahtjevni. Stoga se pribjegava heurističkim i metaheurističkim metodama koje pokušavaju dati dovoljno dobra rješenja.

Glavna dva načina poravnavanja su:
\begin{enumerate}
	\item Globalno poravnavanje - vrsta globalne optimizacije koja poravnava slijedove proizvodeći pritom rezultantni slijed čija je duljina jednaka najdužem ulaznom slijedu
	\item Lokalno poravnavanje  - ova vrsta poravnavanja identificira regije sličnosti u ulaznim slijedovima(koji često mogu biti divergentni, tj. veoma različiti bilo strukturalno, funkcijski ili evolucijski). Stoga su lokalna poravnanja "cijenjenija", no i računalno intenzivnija za izvesti.
	\item Hibridne metode - također poznate pod nazivima \engl{semi-global or glocal} su, naravno, kombinacija globalnih i lokalnih algoritama na način da pokušavaju pronaći najbolje moguće djelomično poravnanje slijedova
\end{enumerate}

Prilikom izrade poravnanja para slijedova glavne metode/tehnike poravnavanja su:
\begin{enumerate}
	\item Optimalno - Dinamičko programiranje(globalna varijanta se zove Needleman-Wunsch algoritam, a lokalna Smith-Waterman algoritam).
	\item Vizualna optimalna metoda - Graf ponavljanja. Na jednoj osi se nalazi prvi slijed, a na drugoj drugi(matrica je N-dimenzionalna). U matrici koju razapinju te 2(N) osi se stavlja točka u onim ćelijama gdje se znakovi u tom retku i stupcu podudaraju. Na ovaj način možemo vidjeti indekse i(indeks retka, tj. indeks u 1. slijedu) te j(indeks stupca, tj. indeks u 2. slijedu) na kojima se nalaze podudaranja tj. točkice.
	\item Heuristički - Metode riječi/k-torka metode. Posebice se koriste za pretragu velikih baza podataka, gdje će samo par slijedova biti mogući kandidati, pa je potrebno moći na brzi način ispitati je li slijed uopće kandidat. Ukoliko se ustanovi da je slijed mogući kandidat, ulazi se u detaljnije ispitivanje. 
	
	Vrlo popularni alati koji koriste ove metode su FASTA i BLAST familija alata. Sama metoda dijeli dugački slijed na podslijedove("riječi") duljine k znakova, te zatim izračunava i uspoređuje ofsete. Ukoliko se detektira da više različitih riječi proizvodi isti ofset, to znači da postoji moguća regija podudaranja koju se onda treba detaljnije ispitati.

Implementacije FASTA i BLAST algoritama su dostupne na: \href{https://www.ebi.ac.uk/Tools/sss/fasta/}{EMBL FASTA} i \href{https://blast.ncbi.nlm.nih.gov/Blast.cgi}{NCBI-BLAST}.
\end{enumerate}

Jedan od načina ispitivanja i utvrđivanja korisnosti poravnanja para slijedova je pomoću takozvanog najdužeg jedinstvenog podudaranja\engl{MUM - Maximal Unique Match}. Radi se o podslijedu, koji je prisutan u oba slijeda koje želimo poravnati. Primjer algoritma koji koristi MUM je MUMmer. Značenje pojedinih dijelova kratice MUM:
\begin{enumerate}
	\item Podudaranje - ovaj podslijed(tj. MUM) se nalazi u oba slijeda
	\item Jedinstveno - ovaj podslijed se pojavljuje samo jedanput u oba ulazna slijeda
	\item Najduže - ovaj podslijed je najduži od svih podslijedova koji ispunjavaju gornja 2 uvjeta
\end{enumerate}

Identificirani MUM se može koristiti kao "sidro" pri poravnavanju slijedova, te može ukazivati na nekakvu vrstu evolucijske sličnosti.

\begin{figure}[h]
	\centerline{\includegraphics[max width=\textwidth]{slike/Zinc-finger-dot-plot.png}}
	\caption{Primjer grafa ponavljanja - DNA ljudskog transkripcijskog faktora cinknog prsta ~\cite{slika-Zinc-finger-dot-plot}}
	\label{fig:Zinc-finger-dot-plot}
\end{figure}
\begin{figure}[h]
	\centerline{\includegraphics[width=\textwidth]{slike/Mup_locus_showing_DNA_repeats.jpg}}
	\caption{Primjer grafa ponavljanja - Samo-usporedba dijela genoma vrste miša. Na grafu ponavljanja vidimo određene karakteristike DNA, poput ponavljanja, invertirajućih ponavljanja te indela~\cite{slika-Mup_locus_showing_DNA_repeats}}
	\label{fig:Mup_locus_showing_DNA_repeats}
\end{figure}

\chapter{Poravnavanje više slijedova}
Poravnanje više slijedova je puno zahtjevnije od poravnavanja 2 slijeda, pošto je potrebno istovremeno poravnati N slijedova. Iako veoma zahtjevno, MSA i tehnike njenog rješavanja koriste se i u ne-biološkim područjima. U samoj biologiji se koriste za identificiranje evolucijski očuvanih dijelova DNA, RNA i proteina, traženje katalitičkih aktivnih mjesta enzima, konstrukciju filogenetskih stabala, itd...
Kod MSA se posebice pribjegava heurističkim rješenjima i metodama, pošto je ona izuzetno računalno intenzivna te spada u skup NP-kompletnih kombinatornih optimizacijskih problema.

Kod heurističkih metoda se pokušava odabrati nekakva heuristika s uvidom u sam proces evolucije kako bi poravnanje bilo što prirodnije i logičnije, pa većina heuristika oponaša samu evoluciju.
Sva moguća različita poravnanja više slijedova mogu se prikazati kao zasebna stanja(vrhovi) u grafu. Na bridovima koji međusobno povezuju vrhove se nalaze težine koje su određene heuristikom. Prilikom određivanja najboljeg mogućeg poravnanja koriste se "tragovi", a prilikom njihovog odabira biraju se bridovi tako da se maksimizira korisnost/sličnost prema nekom konkretnom kriteriju kojega definira cilj/svrha poravnavanja.

Metode i rješenja za poravnavanje više slijedova odjednom su:
\begin{enumerate}
	\item Optimalno - Dinamičko programiranje - isto kao i u slučaju poravnavanja para slijedova, samo što je u ovom slučaju potrebno konstruirati  N-dimenzionalne matrice te s njima raditi algoritme slične onima koji se koriste pri poravnanju para slijedova. Zbog izuzetne memorijske i vremenske složenosti dinamičko programiranje se ne koristi kada je N > 4.
	\item Optimalno - Matematičko programiranje, tj. specifično linearno programiranje se isto koristi jer je efikasnije od dinamičkog programiranja. Moguće je MSA razbiti na manje dijelove i iterativno rješavati sve dok se ne pronađe optimalno rješenje. Primjeri algoritama koji se koriste su grana i cijena\engl{Branch and price} te Benderova dekompozicija\engl{Benders' decomposition}
	\item Eksperimentalno - kvantna računala koja pokazuju visoku efikasnost u poravnavanju više slijedova
	\item Suboptimalno - Progresivne metode - još se nazivaju i hijerarhijske ili stablaste metode. Rade tako što prvo naprave poravnanje para slijedova, i to 2 najsličnija slijeda. Zatim se redom poravnavaju drugi, manje slični(evolucijski udaljeniji) slijedovi sa rezultantnim slijedom, i tako dok se svi početni slijedovi ne asimiliraju u konačan MSA slijed. Primjeri algoritama su Clustal implementacije i T-Coffee.
	
	Glavni problem ovih algoritama su propagirajuće greške, koje kada se jednom jave se propagiraju sve do kraja algoritma te time mogu uvelike utjecati na krajnji rezultantni slijed.
	\item Suboptimalno - Iterativne metode - slične su progresivnim metodama, no pokušavaju poboljšati nedostatak progresivnih metoda - odabir početna 2 slijeda nad kojima se izvršava poravnavanje para slijedova. To se radi tako da se optimizira ciljna funkcija te redoslijed poravnavanja.
	\item Suboptimalno - Pronalazak motiva - ova metoda se temelji na pokušavanju poravnavanja kratkih, ali očuvanih podslijedova(poznatih kao motivi - slijed nukleotida ili amino-kiselina koji je čest i za kojega se misli da je povezan s određenom biološkom funkcijom).
	\item Suboptimalno - Skriveni Markovljevi Modeli\engl{HMM - Hidden Markov Models} koji se koriste za proizvodnju cijele lepeze mogućih MSA za ulazne slijedove. Iako su u svojim začecima davali relativno loše rezultate, nakon nekog vremena su postali posebno efektivni za nišu poravnavanja slijedova koji su u dalekom srodstvu(HMM su otporniji na "šumove" u slijedovima koje stvaraju konzervativne ili polu-konzervativne metode).
	\item Suboptimalno - Genetski algoritmi - koriste nekakvu funkciju nagrade(dosta često se koristi zbroj parova)
	\item Suboptimalno - Simulirano kaljenje - koristi nekakvu funkciju nagrade(dosta često se koristi zbroj parova)
	\item Suboptimalno - Burrows-Wheelerova transformacija(kompresija sortiranja blokova) i FM-Indexi - koriste se za brzo poravnavanje kratkih očitanja. Neki od alata koji ih koriste su npr: Bowtie i BWA
\end{enumerate}

\begin{figure}[h]
	\centerline{\includegraphics[max width=\textwidth]{slike/A_profile_HMM_modelling_a_multiple_sequence_alignment.png}}
	\caption{Korištenje HMM za poravnavanje više slijedova~\cite{slika-A_profile_HMM_modelling_a_multiple_sequence_alignment}}
	\label{fig:A_profile_HMM_modelling_a_multiple_sequence_alignment}
\end{figure}

\begin{figure}[h]
	\centerline{\includegraphics[width=\textwidth]{slike/Hemagglutinin-alignments.png}}
	\caption{Poravnanje više slijedova - 27 proteinskih slijedova H-Hemaglutinin ptičje gripe~\cite{slika-Hemagglutinin-alignments}}
	\label{fig:Hemagglutinin-alignments}
\end{figure}

\section{Dinamičko programiranje}
Kao i u slučaju 2 slijeda(poravnavanje para slijedova), dinamičko programiranje uvijek da optimalno rješenje. Za provođenje poravnavanja koriste se različite matrice. Za poravnavanje proteina potrebne su 2 stavke:
\begin{enumerate}
	\item Penal za praznine
	\item Matrica substitucija - dodjeljuje određeni broj bodova(ili vjerojatnost) poravnanju između 2 aminokiseline, a s obzirom na njihovu biokemijsku sličnost te evolucijsku udaljenost
\end{enumerate}

U slučaju DNA ili RNA slijedova koriste se slične, ali i dalje različite matrice:
\begin{enumerate}
	\item Penal za praznine
	\item Matrica substitucija - jednostavnija od matrice substitucija proteina po tome što se u ovoj matrici substitucija dodjeljuju bodovi na temelju podudaranja nukleotida
\end{enumerate}

Bodovi u matricama substitucija u slučaju globalnog poravnavanja mogu biti svi pozitivni brojevi, ali mogu biti i mix pozitivnih i negativnih brojeva. U slučaju lokalnoga poravnavanja je potrebno imati i pozitivne i negativne brojeve kao bodove.

Kada se radi dinamičko programiranje za poravnavanje para slijedova, potrebno je konstruirati 2D matricu za memoizaciju, a u slučaju poravnavanja više slijedova, potrebno je konstruirati N-dimenzionalnu matricu za memoizaciju. U procesu memoizacije se postepeno gradi tablica poravnanja, a u toj tablici svaka ćelija ovisi o rezultatima samo prethodnih ćelija. U ćeliju se može upisati više vrijednosti, pa se odabire ona vrijednost koja najviše zadovoljava odabrani kriterij(najčešće maksimizacija ili minimizacija vrijednosti). Kod poravnavanja para slijedova, 2D matrica ima 2 osi(x i y, ili i te j), a na svaku os se stavlja po 1 ulazni slijed koji se poravnava. U slučaju N slijedova, imamo dakle i N osi te je stoga matrica N-dimenzionalna. Složenost u slučaju poravnavanja para slijedova je \[ O(Length(Slijed1)  * Length(Slijed2)) \], pa je složenost u slučaju poravnavanja N slijedova \[ O(Length^{N}) \].

\begin{figure}[h]
	\centerline{\includegraphics[max width=\textwidth]{slike/An-illustration-of-how-the-Needleman-Wunsch-algorithm-can-be-computed-using-dynamic.png}}
	\caption{Prikaz 2D matrice za poravnavanje para slijedova u postupku dinamičkog programiranja~\cite{slika-An-illustration-of-how-the-Needleman-Wunsch-algorithm-can-be-computed-using-dynamic}}
	\label{fig:An-illustration-of-how-the-Needleman-Wunsch-algorithm-can-be-computed-using-dynamic}
\end{figure}

\begin{figure}[h]
	\centerline{\includegraphics[max width=\textwidth]{slike/2_slijeda_dp.png}}
	\caption{Postupak praćenja unazad primijenjen za dobivanje konačnog poravnanja u postupku dinamičkog programiranja~\cite{slika-2_slijeda_dp}}
	\label{fig:2_slijeda_dp}
\end{figure}

Gornji pristup proširen na N-dimenzionalnu verziju je pokazan da je NP-kompletan problem. Altschul je 1989. godine, temeljeno na Carillo-Lipman algoritmu, predložio kako ubrzati MSA tako da restringira domenu, pa se ne pretražuje cijeli N-dimenzionalni prostor. Umjesto toga, radi se poravnavanje svakog para slijedova među ulaznim slijedovima, te se zatim koristi N-dimenzionalni presjek ovih poravnanja kako bi se našlo konačno N-dimenzionalno MSA poravnanje. Funkcija optimizacije koja se koristi u ovoj inačici optimizira zbroj svih parova karaktera na svakoj poziciji u poravnanju, takozvano bodovanje zbroja parova. Ovakva inačica je i implementirana u programskim paketima za poravnavanje više slijedova. Nadalje, 2019. godine Hosseininasab i van Hoeve su uspjeli dokazati mogućnost dobivanja poravnanja više slijedova u polinomijalnoj vremenskoj složenosti koristeći dijagrame odluka. 

\section{Progresivne metode}
Ova metoda se temelji na heurističkoj metodi poznatoj kao progresivna tehnika(ili hijerarhijska ili stablasta metoda) koju su 1987. godine razvili Da-Fei Feng i Doolittle. Ova vrsta poravnavanja gradi konačno poravnanje tako da postepeno radi poravnanje parova slijedova sve dok se u konačan slijed ne "inkorporiraju" svi ulazni slijedovi. Kreće se od najsličnijih ulaznih slijedova prema evolucijski udaljenijima. Primjena ove metode sastoji se od 2 faze:
\begin{enumerate}
	\item 1. Faza - izgradnja stabla navođenja\engl{Guide Tree} koje će se koristiti u 2. fazi. Ovo stablo u svojoj suštini sadrži i opisuje odnose među ulaznim slijedovima. Ovo stablo gradi se pomoću nekakve efikasne metode grupiranja, poput UPGMA ili spajanja susjeda. Jedan od načina određivanja težina je broj identičnih dvo-slovnih podslijedova(poput FASTA).
	\item 2. Faza - postepena izgradnja konačnog poravnanja koristeći stablo navođenja izgrađeno u 1. fazi. 
\end{enumerate}

Ove metode su heurističke i ne daju optimalna rješenja. Njihov glavni nedostatak je propagacija grešaka ukoliko se one dogode u bilo kojoj fazi izgradnje MSA, a također loše performiraju ukoliko su ulazni slijedovi u dalekom srodstvu. Stoga mnogi algoritmi progresivnih metoda koriste dodatnu sekundarnu funkciju koja pridružuje faktore skaliranja pojedinim ulaznim slijedovima koji se poravnavaju - oni se pridružuju na ne-linearan način, a temeljeno na filogenetskoj udaljenosti kako bi se "nadoknadilo" za ne-nasumičan odabir ulaznih sekvenci koje se poravnavaju.

Zbog svoje visoke efikasnosti progresivne metode mogu istovremeno poravnavati stotine, pa čak i tisuće slijedova. Ove i iterativne metode su najraširenije tehnike MSA, te neki od najpopularnijih bioinformatičkih alata spadaju upravo u ove kategorije. Upravo zbog toga razloga, često su alati dostupni i na web stranicama(bez potrebe za lokalnom instalacijom, pošto su hostani na serverima). Na primjer, ClustalW je dostupan na stranicama \href{https://web.archive.org/web/20060705082556/http://align.genome.jp/}{GenomeNet}, \href{http://www.ebi.ac.uk/Tools/clustalw2/index.html}{EBI} i \href{http://www.ch.embnet.org/software/ClustalW.html}{EMBNet}. Inače mnogo bioinformatičkih, bioloških i medicinskih resursa se mogu naći na stranicama laboratorija i instituta: \href{https://www.ebi.ac.uk/}{European Bioinformatics Institute}, \href{https://www.ncbi.nlm.nih.gov/}{National Center for Biotechnology Information - National Library of Medicine - National Institutes of Health} i vjerojatno neki drugi. Najpoznatiji od algoritama su Clustal familija algoritama(ClustalW, ClustalW2 i najnoviji ClustalOmega), MAFFT(MSA koristeći FFT tj. brzu Fourierovu transformaciju), T-Coffee, PSAlign, itd... Trenutno ClustalOmega je jedan od najpopularnijih, najbržih i najkvalitetnijih algoritama.

ClustalW se koristi za konstrukciju filogenetskih stabala(unatoč tome što autor savjetuje da se neuređena poravnanja ne bi trebala koristiti u takve svrhe) i za predviđanje proteinskih struktura pomoću modeliranja homologije. ClustalOmega je relativno novi algoritam, a temelji se na seeded stablima navođenja i dvo-profilnim skrivenim Markovljevim modelima. T-Coffee, iako sporiji od Clustal familije algoritama, je generalno efektivniji u MSA kada su ulazni slijedovi u udaljenom srodstvu. T-Coffee koristi Clustal i LALIGN izlaze za svoj algoritam. Progresivne metode su heurističke, dakle neće dati sasvim optimalna rješenja(za razliku od, npr. dinamičkoga programiranja). Razvijena je jedna polu-progresivna metoda koja se temelji na teoriji grafova, te pretrazi grafova - PSAlign. On se ne temelji na heuristici koja gubi informacije, a i dalje se pokreće u polinomijalnoj vremenskoj složenosti.

\begin{figure}[h]
	\centerline{\includegraphics[max width=\textwidth]{slike/Clustal_Omega_Algorithm_Flowchart.png}}
	\caption{Dijagram toka algoritma Clustal Omega~\cite{slika-Clustal_Omega_Algorithm_Flowchart}}
	\label{fig:Clustal_Omega_Algorithm_Flowchart}
\end{figure}

\begin{figure}[h]
	\centerline{\includegraphics[max width=\textwidth]{slike/Profile_Hidden_Markov_Model.png}}
	\caption{Struktura HMM-a profila, kakvi su korišteni u Clustal Omega~\cite{slika-Profile_Hidden_Markov_Model.png}}
	\label{fig:Profile_Hidden_Markov_Model.png}
\end{figure}


\section{Iterativne metode}
Još jedna skupina mogućih metoda MSA su i iterativne metode. Djeluju slično progresivnim metodama, no razlika je što one stalno poravnavaju početni slijed, ali i dodaju nove slijedove rastućem MSA. Progresivne metode su jako ovisne o visokokvalitetnim početnim poravnanjima jer su ta poravnanja uvijek ugrađena u konačan rezultat, odnosno njihovo poravnanje se više ne razmatra. Stoga možemo reći da progresivne metode poboljšavaju učinkovitost po cijenu točnosti.

Suprotno njima, iterativne metode se mogu vratiti prethodno izračunatim poravnanjima parova ili nekoj podskupini MSA koji uključuju podskupove niza upita kao sredstvo za optimizaciju opće ciljne funkcije kao što je pronalaženje rezultata poravnanja visoke kvalitete. 

Brojne iterativne metode su implementirane i dostupne u raznim softverskim paketima no još nije odabran najbolji od njih. Softverski paket PRRN/PRRP koristi algoritam uspona na brdo za optimizaciju svog MSA rezultata poravnanja i iterativno ispravlja poravnanja i lokalno različita područja rastućeg MSA. PRRN najbolje radi kada pročišćava poravnanja prethodno konstruirana nekom bržom metodom.

Nadalje, drugi iterativni program, DIALIGN, se usko fokusira na lokalna poravnanja između podsegmenata bez korištenja penala za praznine. Alternativna metoda koja koristi brza lokalna poravnanja kao sidrišta ili „sjeme“ za sporiji postupak globalnog poravnanja implementirana je u paketu CHAOS / DIALIGN.

Vrijedna spomena je još i metoda pod nazivom MUSCLE koja poravnava višestruke slijedove pomoću očuvanja dnevnika. Ona poboljšava progresivne metode preciznijom mjerom udaljenosti za procjenu povezanosti dviju slijedova.

\section{Skriveni Markovljevi modeli}
Skriveni Markovljevi modeli su vjerojatnosni modeli. Djeluju tako da dodjeljuju vjerojatnost svim mogućim kombinacijama praznina, podudaranja i nepodudaranja kako bi pronašli najvjerojatniji MSA ili skup mogućih MSA (odnosno najbolje poravnanje nizova). Skriveni Markovljevi modeli mogu pronaći jedan najbolji rezultat, ali i generirati obitelj mogućih poravnanja kojima se zatim procjenjuje biološki značaj.

Skriveni Markovljevi modeli mogu izvoditi i globalna i lokalna poravnanja. Iako su metode temeljene na skrivenim Markovljevim lancima relativno nedavno razvijene, nude značajna poboljšanja u računskoj brzini, posebno za slijedove koji sadrže preklapajuća područja.

Metode temeljene na skrivenim Markovljevim modelima djeluju tako da MSA predstavljaju kao oblik usmjerenog acikličkog grafa koji se sastoji od niza čvorova koji predstavljaju moguće unose u stupce MSA. U ovom načinu prikazivanja, stupac koji je u potpunosti sačuvan(kojem svi slijedovi u MSA dijele isti znak na istomu mjestu) kodiran je kao jedan čvor s onoliko odlaznih veza koliko je mogućih znakova u sljedećem stupcu poravnanja. Tipično, kod skrivenih Markovljevih modela, promatrana stanja su pojedinačni stupci poravnanja, a „skrivena“ stanja pretpostavljeni slijed predaka iz kojeg se pretpostavlja da su nizovi u trenutnom skupu nastali. 

Viterbi algoritam je učinkoviti način pretraživanja metodom dinomičkog programiranja. Koristi se za slijedno poravnavanje rastućeg MSA prilikom dobivanja sljedećeg niza. Tu se očitava razlika u odnosu na progresivne metode jer se poravnanja prethodnih slijedova ažuriraju prilikom dodavanja svakog novog slijeda(sekvence). Međutim, kao i kod progresivnih metoda, i na ovu tehniku poravnavanja može utjecati redoslijed kojim su nizovi u skupu dodavani u poravnanje.

Dostupno je nekoliko softverskih programa u kojima su implementirane varijante metoda temeljenih na skrivenim Markovljevim lancima i koje su poznate po svojoj skalabilnosti i učinkovitosti bez obzira što je pravilna upotreba skrivenih Markovljevih lanaca složenija od korištenja uobičajenih progresivnih metoda. Najjednostavnija od njih je POA\engl{Partial-Order Alignment}. Slična njoj, ali puno općenitija metoda implementirana je u paketima SAM\engl{Sequence Alignment and Modeling System} i HMMER. Također, postoji i HHsearch. To je softverski paket koji se koristi za otkrivanje evolucijski udaljenih proteinskih slijedova koji je temeljen na usporedbi parova skrivenih Markovljevih lanaca. Poslužitelj koji je izvodio HHsearch bio je daleko najbrži od 10 najboljih poslužitelja za automatsko predviđanje strukture na natjecanjima CASP7 i CASP8.

\chapter{Zaključak i sažetak}
Problem poravnavanja više slijedova je izuzetno veliki izazov - algoritmi i postupci koji daju optimalna rješenja jednostavno nisu dovoljno efikasni i uporabljivi u primjeni, a pogotovo kada se uzme u obzir da su biološki slijedovi često izuzetno dugački - na primjer, ljudski genom je duljine do ~3.2 milijarde parova baza. Gledajući memorijsku i vremensku složenost dinamičkoga programiranja(dužina slijeda na n. potenciju) vidljivo je da je složenost jednostavno prevelika, a pogotovo kada se radi poravnavanje stotine ili čak tisuće slijedova! Stoga se pribjegava heurističkim metodama(ili čak i metodama temeljenima na teoriji grafova). Ove metode, iako ne daju optimalna rješenja, daju rješenja koja su i dalje velike kvalitete te služe svrsi. Dostupan je velik broj alata i algoritama koji se natječu u poravnavanju slijedova, pa često i različiti algoritmi pokrivaju različite niše te bioinformatičke svrhe i zahtjeve.

S mogućnošću poravnavanja stotina ili čak tisuća slijedova odjednom, uvelike se olakšava postupak konstrukcije filogenetskih stabala, predikcije ekspresije gena, struktura proteina ili nukleinskih kiselina, otkrivanje bioloških i strukturalnih funkcija, otkrivanje evolucijskih veza, otkrivanje aktivnih i vezivajućih mjesta, itd... Poravnavanje slijedova je jedan od kamena temeljaca bioinformatike, pa je veoma važno imati razvijene algoritme koji mogu na efikasan način ju provoditi. Cijelo vrijeme se pokušava inovirati, te izlaze razni radovi i obavljaju se istraživanja na temama poravnavanja para slijedova i poravnavanja više slijedova - pa se tako javljaju i skriveni Markovljevi modeli, metode temeljene na konsenzusu slijedova, metode sa znanjem filogenije, pronalaskom motiva(analiza profila), pronalazak mjesta povezivanja transkripcijskih faktora(poravnanje više slijedova s obzirom na nekodirajuće dijelove, npr. mjesta povezivanja transkripcijskih faktora), genetski algoritmi i simulirano kaljenje, matematičko programiranje(linearno programiranje, tj. simpleks metoda), kvantno računanje, ...

Trenutno jedan od najpopularnijih MSA algoritama je ClustalOmega, koji je progresivna metoda. Progresivne i iterativne metode su najpopularniji načini rješavanja problema poravnavanja. MSA je NP-kompletan problem, te algoritmi koji pribježu heuristikama moraju paziti kako ne bi zalutali u lokalne optimume. Zato se često koristi načelo "dosljednosti"\engl{consistency} prilikom kojega se bodovi za poravnanja parova slijedova reevaluiraju prema nekakvoj funkciji cilja koja procjenjuje koliko su ta poravnanja parova slijedova prilagođena/sposobna dati kvalitetno globalno MSA rješenje. T-Coffee je klasičan predstavnik MSA progresivnih metoda baziranih na dosljednosti.

\bibliography{literatura}
\bibliographystyle{fer}

\end{document}
